﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;

namespace Tesseract_OCR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btOCR_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = openFileDialog.FileName;
                pictureChose.Image = Image.FromFile(txtPath.Text);
                using (frmWait frm = new frmWait(ORC_Excute))
                {
                    frm.ShowDialog(this);
                }
            }
        }
        
        void ORC_Excute()
        {
            try
            {
                var bitmap = new Bitmap(txtPath.Text);
                var bitmapConvert = GrauwertBild(bitmap);
                pictureChose.Image = bitmapConvert;
                using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.TesseractAndCube))
                {
                    using (var img = PixConverter.ToPix(bitmap))
                    {
                        using (var page = engine.Process(img,PageSegMode.SingleBlock))
                        {
                            string str = page.GetText();
                            str = str.Replace("\n\n", "\r\n").Replace("\n","\r\n");
                            txtResult.Text = str;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Have problem with detect words, please try again");
            }
        }

        public Bitmap GrauwertBild(Bitmap input)
        {
            Bitmap greyscale = new Bitmap(input.Width, input.Height);
            for (int x = 0; x < input.Width; x++)
            {
                for (int y = 0; y < input.Height; y++)
                {
                    Color pixelColor = input.GetPixel(x, y);
                    //  0.3 · r + 0.59 · g + 0.11 · b
                    int grey = (int)(pixelColor.R * 0.3 + pixelColor.G * 0.59 + pixelColor.B * 0.11);
                    greyscale.SetPixel(x, y, Color.FromArgb(pixelColor.A, grey, grey, grey));
                }
            }
            return greyscale;
        }

        public Bitmap GrayScaleFilter(Bitmap image)
        {
            Bitmap grayScale = new Bitmap(image.Width, image.Height);

            for (Int32 y = 0; y < grayScale.Height; y++)
                for (Int32 x = 0; x < grayScale.Width; x++)
                {
                    Color c = image.GetPixel(x, y);

                    Int32 gs = (Int32)(c.R * 0.3 + c.G * 0.59 + c.B * 0.11);

                    grayScale.SetPixel(x, y, Color.FromArgb(gs, gs, gs));
                }
            return grayScale;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Image files|*.jpg;*.tif;*.png|All files|*.*";
        }
    }
}
